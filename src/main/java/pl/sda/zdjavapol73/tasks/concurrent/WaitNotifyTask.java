package pl.sda.zdjavapol73.tasks.concurrent;

import pl.sda.zdjavapol73.api.Task;

import static pl.sda.zdjavapol73.tasks.concurrent.ConcurrentTask.log;

public class WaitNotifyTask implements Task {
    @Override
    public void run() {
        final Customer customer = new Customer();
        final Thread withDrawThread = new Thread(new WithdrawThread(customer));
        final Thread depositThreadA = new Thread(new DepositThread(customer));
        final Thread depositThreadB = new Thread(new DepositThread(customer));

        withDrawThread.start();
        depositThreadA.start();
        depositThreadB.start();
    }
}

class Customer {
    private int availableAmount = 0;

    synchronized void withdraw(int amountToWithdraw) {
        log("Trying to withdraw " + amountToWithdraw + " PLN");
        while (availableAmount < amountToWithdraw) {
            log("Not enough money! Waiting for transfer!");
            try {
                wait();
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.err.println("Oops");
            }
        }
        log("Withdraw successful!");
    }

    synchronized void deposit(final int amountToDeposit) {
        log("Depositing " + amountToDeposit + " PLN");
        availableAmount += amountToDeposit;
        notify();
    }
}

class WithdrawThread implements Runnable {

    private final Customer customer;

    WithdrawThread(final Customer customer) {
        this.customer = customer;
    }


    @Override
    public void run() {
        customer.withdraw(1000);
    }
}

class DepositThread implements Runnable {
    private final Customer customer;

    DepositThread(final Customer customer) {
        this.customer = customer;
    }

    @Override
    public void run() {
        customer.deposit(500);
    }
}
