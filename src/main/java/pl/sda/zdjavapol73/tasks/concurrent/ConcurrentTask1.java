package pl.sda.zdjavapol73.tasks.concurrent;

import pl.sda.zdjavapol73.api.Task;

import static pl.sda.zdjavapol73.tasks.concurrent.ConcurrentTask.log;

public class ConcurrentTask1 implements Task {
    @Override
    public void run() {
        oneThread();
        multiThread();
    }

    // TODO: find even numbers from 0 to 1_000_000
    private void oneThread() {
        final long start = System.currentTimeMillis();
        log("oneThread() impl start work");
        final EvenNumbersFinder finder1 = new EvenNumbersFinder(0, 20000);
        log("even number from <0, 20000>: " + finder1.findEvenNumbers());

        final long end = System.currentTimeMillis();
        log("oneThread() impl end work in time: " + (end - start));
    }

    // TODO: split set to 4 smaller sets 1_000_00 / 4
    private void multiThread() {
        final long start = System.currentTimeMillis();

        final Thread t1 = new Thread(() -> {
            final EvenNumbersFinder finder = new EvenNumbersFinder(0, 5000);
            log("even number from <0, 5000>: " + finder.findEvenNumbers());
        }, "even number finder in <0, 5000>");

        final Thread t2 = new Thread(() -> {
            final EvenNumbersFinder finder = new EvenNumbersFinder(5001, 10000);
            log("even number from <5001, 10000>: " + finder.findEvenNumbers());
        }, "even number finder in <5001, 10000>");

        final Thread t3 = new Thread(() -> {
            final EvenNumbersFinder finder = new EvenNumbersFinder(10001, 15000);
            log("even number from <10001, 15000>: " + finder.findEvenNumbers());
        }, "even number finder in <10001, 15000>");

        final Thread t4 = new Thread(() -> {
            final EvenNumbersFinder finder = new EvenNumbersFinder(15001, 20000);
            log("even number from <15001, 20000>: " + finder.findEvenNumbers());
        }, "even number finder in <15001, 20000>");

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        try {
            t1.join(10_000);
            t2.join(10_000);
            t3.join(10_000);
            t4.join(10_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final long end = System.currentTimeMillis();
        log("multiThread() impl end work in time: " + (end - start));
    }

    // TODO: return Thread which is responsible for finding even numbers from start to end
    private Thread createThread(int start, int end) {
        return null;
    }
}
