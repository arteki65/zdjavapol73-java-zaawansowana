package pl.sda.zdjavapol73.tasks.collection;

import java.util.Arrays;

public class SDAArrayList<T> {

    private Object[] array = new Object[5];

    public boolean add(T el) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                array[i] = el;
                return true;
            }
        }
        return false;
    }

    public T remove(int i) {
        if (i > array.length) {
            throw new UnsupportedOperationException("i > array.length");
        }
        final T removedEl = (T) array[i];
        array[i] = null;
        return removedEl;
    }

    public T get(int i) {
        if (i > array.length) {
            throw new UnsupportedOperationException("i > array.length");
        }
        return (T) array[i];
    }

    public void display() {
        System.out.println("List:" + Arrays.toString(array));
    }
}
