package pl.sda.zdjavapol73.tasks.nio;

import pl.sda.zdjavapol73.api.Task;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.stream.Collectors;

public class NioTask implements Task {

    private final String path = "/home/arek/Pictures/";

    @Override
    public void run() {
        Path picturesPath = Paths.get(path);
        /*printPathInfo(picturesPath);
        System.out.println();
        printFileInfo(picturesPath.toFile());*/
        task33(picturesPath);
    }

    private void task33(Path picturesPath) {
        System.out.println("All .png and .jpg in dir and subdirs of " + picturesPath);
        listPicturesInDirectory(picturesPath);
    }

    private void listPicturesInDirectory(Path path) {
        if (path.toFile().isDirectory()) {
            File[] pictureFiles = path.toFile().listFiles((file) -> {
                if (file.isFile()) {
                    return file.getName().endsWith(".png") || file.getName().endsWith(".jpg");
                }
                return false;
            });

            if (pictureFiles != null) {
                System.out.println("Pictures in dir " + path + "\n" +
                        Arrays.stream(pictureFiles).map(File::getName).collect(Collectors.joining("\n")));
                System.out.println();
            }

            File[] subDirs = path.toFile().listFiles(File::isDirectory);
            if (subDirs != null) {
                for (File dir : subDirs) {
                    listPicturesInDirectory(dir.toPath());
                }
            }
        }
    }

    private void printPathInfo(Path path) {
        System.out.println("path.getFileSystem() -> " + path.getFileSystem());
        System.out.println("path.getFileName() -> " + path.getFileName());
        System.out.println("path.getNameCount() -> " + path.getNameCount());
        System.out.println("path.isAbsolute() -> " + path.isAbsolute());
        System.out.println("path.getParent() -> " + path.getParent());
        System.out.println("path.getRoot() -> " + path.getRoot());
    }

    private void printFileInfo(File file) {
        System.out.println("file.length() -> " + file.length());
        System.out.println("file.list() -> " + Arrays.toString(file.list()));
        System.out.println("file.listFiles() -> " + Arrays.toString(file.listFiles((dir, name) -> true)));
        System.out.println("file.canExecute() -> " + file.canExecute());
        System.out.println("file.canRead() -> " + file.canRead());
        System.out.println("file.canWrite() -> " + file.canWrite());
        System.out.println("file.getFreeSpace() -> " + file.getFreeSpace());
        System.out.println("file.getUsableSpace() -> " + file.getUsableSpace());
        System.out.println("file.getTotalSpace() -> " + file.getTotalSpace());
        System.out.println("file.isFile() -> " + file.isFile());
        System.out.println("file.isHidden() -> " + file.isHidden());
        System.out.println("file.lastModified() -> " +
                LocalDateTime.ofEpochSecond(file.lastModified() / 1000, 0, ZoneOffset.ofHours(0)));
    }
}
