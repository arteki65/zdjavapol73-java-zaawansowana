package pl.sda.zdjavapol73.tasks.concurrent;

import pl.sda.zdjavapol73.api.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static pl.sda.zdjavapol73.tasks.concurrent.ConcurrentTask.log;

public class ConcurrentTask1WithExecutorService implements Task {
    @Override
    public void run() {
        final int cpus = Runtime.getRuntime().availableProcessors();
        log("Available cpus: " + cpus);

        final long start = System.currentTimeMillis();
//        ExecutorService executor = Executors.newFixedThreadPool(3);
//        ExecutorService executor = Executors.newSingleThreadExecutor();
        ExecutorService executor = Executors.newCachedThreadPool();
        try {
            Collection<Future<Collection<Integer>>> futures = executor.invokeAll(getTasks());
            for (Future<Collection<Integer>> f : futures) {
                try {
                    f.get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            final long end = System.currentTimeMillis();
            log("Execution time: " + (end - start));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        executor.shutdown();
    }

    private Collection<Callable<Collection<Integer>>> getTasks() {
        final Callable<Collection<Integer>> r1 = () -> {
            log("Even number finder in <0, 5000>");
            final EvenNumbersFinder finder = new EvenNumbersFinder(0, 500000);
            return finder.findEvenNumbers();
        };

        final Callable<Collection<Integer>> r2 = () -> {
            log("Even number finder in <5001, 10000>");
            final EvenNumbersFinder finder = new EvenNumbersFinder(500001, 1000000);
            return finder.findEvenNumbers();
        };

        final Callable<Collection<Integer>> r3 = () -> {
            log("Even number finder in <10001, 15000>");
            final EvenNumbersFinder finder = new EvenNumbersFinder(1000001, 1500000);
            return finder.findEvenNumbers();
        };

        final Callable<Collection<Integer>> r4 = () -> {
            log("Even number finder in <15001, 20000>");
            final EvenNumbersFinder finder = new EvenNumbersFinder(1500001, 2000000);
            return finder.findEvenNumbers();
        };

        return Arrays.asList(r1, r2, r3, r4, () -> {
            log("Even number finder in <15001, 20000>");
            final EvenNumbersFinder finder = new EvenNumbersFinder(1500001, 2000000);
            return finder.findEvenNumbers();
        });
    }
}
