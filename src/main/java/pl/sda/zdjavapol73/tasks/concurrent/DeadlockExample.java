package pl.sda.zdjavapol73.tasks.concurrent;

import pl.sda.zdjavapol73.api.Task;

import static pl.sda.zdjavapol73.tasks.concurrent.ConcurrentTask.log;

public class DeadlockExample implements Task {
    @Override
    public void run() {
        final String r1 = "r1";
        final String r2 = "r2";

        Thread t1 = new Thread(() -> {
            synchronized (r1) {
                log("Thread 1: Locked r1");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
                synchronized (r2) {
                    log("Thread 1: Locked r2");
                }
            }
        });

        Thread t2 = new Thread(() -> {
            synchronized (r2) {
                log("Thread 2: Locked r2");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
                synchronized (r1) {
                    log("Thread 2: Locked r1");
                }
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log("Exiting? No I will never reach this line of code because threads will NOT join");
    }
}
