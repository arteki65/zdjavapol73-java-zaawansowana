package pl.sda.zdjavapol73.tasks.concurrent;

import pl.sda.zdjavapol73.api.Task;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ConcurrentTask implements Task {
    public static void log(String msg) {
        System.out.println(
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")) + "|" + Thread.currentThread() +
                        "| " + msg);
    }

    @Override
    public void run() {
        log("Hello from concurrent Task!");
        log("Current thread: " + Thread.currentThread());

        Runnable myRunnable = () -> {
            log(Thread.currentThread() + " is going to sleep for 0.5 seconds...");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log(Thread.currentThread() + " woke up and ended work!");
        };
        Thread myThread = new Thread(myRunnable, "myThread");
        myThread.start();

        try {
            myThread.join(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log("Clean up.");
    }
}
