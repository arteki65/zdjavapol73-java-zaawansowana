package pl.sda.zdjavapol73.tasks.concurrent;

import pl.sda.zdjavapol73.api.Task;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static pl.sda.zdjavapol73.tasks.concurrent.ConcurrentTask.log;

public class ScheduledExecutorTask implements Task {
    @Override
    public void run() {
        ScheduledExecutorService executorService =
                Executors.newScheduledThreadPool(1);

        executorService.scheduleAtFixedRate(() -> log("Hello, I am scheduled!"), 0, 1, TimeUnit.SECONDS);

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}
