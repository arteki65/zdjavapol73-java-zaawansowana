package pl.sda.zdjavapol73.tasks.concurrent;

import pl.sda.zdjavapol73.api.Task;

import java.util.Random;

import static pl.sda.zdjavapol73.tasks.concurrent.ConcurrentTask.log;

public class SynchronizationTask implements Task {
    @Override
    public void run() {
        final Pair pair = new Pair(0, 0);
        new Thread(new DummyPairIncrementer(pair), "t1").start();
        new Thread(new DummyPairIncrementer(pair), "t2").start();
    }
}

class Pair {
    private Integer left;
    private Integer right;

    public Pair(final Integer left, final Integer right) {
        this.left = left;
        this.right = right;
    }

    public synchronized void incrementLeft() {
        left++;
    }

    public synchronized void incrementRight() {
        right++;
    }

    public Integer getLeft() {
        return left;
    }

    public Integer getRight() {
        return right;
    }
}

class DummyPairIncrementer implements Runnable {
    private Pair pair;

    public DummyPairIncrementer(final Pair pair) {
        this.pair = pair;
    }

    @Override
    public void run() {
        for (int idx = 0; idx < 100; idx++) {
            pair.incrementLeft();
            pair.incrementRight();
        }
        log(pair.getLeft() + " " + pair.getRight());
    }
}
