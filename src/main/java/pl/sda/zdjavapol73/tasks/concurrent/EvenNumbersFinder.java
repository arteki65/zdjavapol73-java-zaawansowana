package pl.sda.zdjavapol73.tasks.concurrent;

import pl.sda.zdjavapol73.tasks.collection.domain.Season;

import java.util.ArrayList;
import java.util.Collection;

public class EvenNumbersFinder {

    private final int start;

    private final int end;

    public EvenNumbersFinder(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public Collection<Integer> findEvenNumbers() {
        Collection<Integer> result = new ArrayList<>();
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0 && i != 0) {
                result.add(i);
            }
        }
        return result;
    }
}
